import datetime
from bs4 import BeautifulSoup
import requests

from SecNewsNotify.News import News



def get_last_news():
	url = 'http://www.niebezpiecznik.pl'
	response = requests.get(url)

	html = BeautifulSoup(response.text)

	last_news = html.select('div#main div.post div.title h2 a')[1]
	news = News(
		ttl=last_news.attrs['title'],
		lnk_url=last_news.attrs['href']
	)

	return news


def compare_last_news(news=None):

	last_news_file = open('last_news.txt', 'r')
	last_news_file_content = last_news_file.readline()

	return news.link_url.strip() == last_news_file_content


def sendNotification(news):
	requests.post(
		'https://api.pushover.net/1/messages.json',
		data={
			'token': 'ayyr194kwwh7awzyj4mfrjjjs2f6vg',
			'user': 'ug85cc9u2i5zj1nrpmj8n85ismmvoh',
			'message': news.title,
			'title': 'Niebezpiecznik.pl -> nowy artykuł',
			'url': news.link_url,
			'url_title': 'Przejdź do artykułu'
		}
	)


urls_are_the_same = compare_last_news(get_last_news())

if not urls_are_the_same:
	updated_news_file = open('last_news.txt', 'w')
	updated_news_file.write(get_last_news().link_url)
	updated_news_file.close()
	print(datetime.datetime.now())
	print('Link overwritten.')

	sendNotification(get_last_news())
	print('Notification sent')
else:
	print(datetime.datetime.now())
	print('Link is up-to-date.')