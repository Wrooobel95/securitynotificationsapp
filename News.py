
class News(object):

	title: str
	link_url: str

	def __init__(self, ttl: str, lnk_url: str) -> object:
		self.title = ttl
		self.link_url = lnk_url
